I choose Monolithic Architecture for MVP.
pros: faster and cheaper development, easier to understand.
cons: lower reliability than in microservices approach, may be harder to bring new features in future

layers:
 - Presentation layer (front-end);
 - application layer (controllers and services);
 - persistance layer (repositories);
 - data layer (database).

Architecture assume using 2 protocols for both staff and users:
  HTTP for access to available services: booking, payment management
  Websocket for notifications: 
    for staff about new orders,
    for users about orders status, new publications in user profile.

